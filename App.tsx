/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useState} from 'react';
import {SafeAreaView, StyleSheet, Text, TextInput} from 'react-native';
import Speedometer from './src';

const App = () => {
  const [value, setvalue] = useState(0);

  const onChange: (text: string) => void = text => {
    setvalue(parseInt(text) || 0);
  };

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.heading}>SpeedoMeter Test</Text>
      <TextInput
        placeholder="Speedometer Value"
        style={styles.textInput}
        onChangeText={onChange}
        keyboardType="numeric"
      />
      <Speedometer value={value} />
    </SafeAreaView>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  heading: {
    marginVertical: 50,
    paddingHorizontal: 20,
    fontSize: 35,
  },
  textInput: {
    // backgroundColor: 'black',
    // height: 40,
    borderBottomWidth: 0.3,
    borderBottomColor: 'black',
    // height: 25,
    fontSize: 16,
    marginBottom: 50,
    marginHorizontal: 20,
  },
});
