import React from 'react';
import {
  Animated,
  Easing,
  Image,
  ImageStyle,
  StyleProp,
  View,
  ViewStyle,
} from 'react-native';
import Label from './interfaces/Label';
import style, {width as deviceWidth} from './style';
// Utils
import calculateDegreeFromLabels from './utils/calculate-degree-from-labels';
import limitValue from './utils/limit-value';
import validateSize from './utils/validate-size';

interface Props {
  value: number;
  defaultValue?: number;
  size?: number;
  minValue?: number;
  maxValue?: number;
  easeDuration?: number;
  allowedDecimals?: number;
  labels?: Label[];
  needleImage?: any;
  wrapperStyle?: StyleProp<ViewStyle>;
  outerCircleStyle?: StyleProp<ViewStyle>;
  halfCircleStyle?: StyleProp<ViewStyle>;
  imageWrapperStyle?: object;
  imageStyle?: StyleProp<ImageStyle>;
  innerCircleStyle?: StyleProp<ViewStyle>;
}

const PropsDefaultValue: Props = {
  value: 0,
  defaultValue: 50,
  minValue: 0,
  maxValue: 100,
  easeDuration: 500,
  allowedDecimals: 0,
  labels: [
    {
      activeBarColor: '#ffd603',
    },
    {
      activeBarColor: '#b1ff59',
    },
    {
      activeBarColor: '#00943e',
    },
  ],
  needleImage: require('./assets/speedometer-needle.png'),
  wrapperStyle: {},
  outerCircleStyle: {},
  halfCircleStyle: {},
  imageWrapperStyle: {},
  imageStyle: {},
  innerCircleStyle: {},
};

const Speedometer: React.SFC<Props> = props => {
  const {
    value,
    defaultValue,
    size,
    minValue,
    maxValue,
    easeDuration,
    allowedDecimals,
    labels,
    needleImage,
    wrapperStyle,
    outerCircleStyle,
    halfCircleStyle,
    imageWrapperStyle,
    imageStyle,
    innerCircleStyle,
  } = props;
  const speedometerValue = new Animated.Value(defaultValue || 0);
  const degree = 180;
  const perLevelDegree = calculateDegreeFromLabels(degree, labels);
  Animated.timing(speedometerValue, {
    toValue: limitValue(value, minValue, maxValue, allowedDecimals),
    duration: easeDuration,
    easing: Easing.linear,
  }).start();

  const rotate = speedometerValue.interpolate({
    inputRange: [minValue || 0, maxValue || 0],
    outputRange: ['-90deg', '90deg'],
  });

  const currentSize = validateSize(size, deviceWidth - 20);
  return (
    <View
      style={[
        style.wrapper,
        {
          width: currentSize,
          height: currentSize / 2,
        },
        wrapperStyle,
      ]}>
      <View
        style={[
          style.outerCircle,
          {
            width: currentSize,
            height: currentSize / 2,
            borderTopLeftRadius: currentSize / 2,
            borderTopRightRadius: currentSize / 2,
          },
          outerCircleStyle,
        ]}>
        {labels &&
          labels.map((level, index) => {
            const circleDegree = 90 + index * perLevelDegree;
            return (
              <View
                key={`${index}-label`}
                style={[
                  style.halfCircle,
                  {
                    backgroundColor: level.activeBarColor,
                    width: currentSize / 2,
                    height: currentSize,
                    borderRadius: currentSize / 2,
                    transform: [
                      {translateX: currentSize / 4},
                      {rotate: `${circleDegree}deg`},
                      {translateX: (currentSize / 4) * -1},
                    ],
                  },
                  halfCircleStyle,
                ]}
              />
            );
          })}
        <Animated.View
          style={[
            style.imageWrapper,
            {
              top: -(currentSize / 15),
              transform: [{rotate}],
            },
            imageWrapperStyle,
          ]}>
          <Image
            style={[
              style.image,
              {
                width: currentSize,
                height: currentSize,
              },
              imageStyle,
            ]}
            source={needleImage}
          />
        </Animated.View>
        <View
          style={[
            style.innerCircle,
            {
              width: currentSize * 0.6,
              height: (currentSize / 2) * 0.6,
              borderTopLeftRadius: currentSize / 2,
              borderTopRightRadius: currentSize / 2,
            },
            innerCircleStyle,
          ]}
        />
      </View>
    </View>
  );
};

Speedometer.defaultProps = PropsDefaultValue;

export default Speedometer;
