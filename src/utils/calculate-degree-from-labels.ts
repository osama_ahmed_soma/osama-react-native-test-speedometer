import Label from '../interfaces/Label';

function calculateDegreeFromLabels(degree: number, labels: Label[] = []) {
  const perLevelDegree = degree / labels.length;
  return perLevelDegree;
}

export default calculateDegreeFromLabels;
