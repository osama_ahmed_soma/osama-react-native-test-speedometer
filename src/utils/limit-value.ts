function limitValue(
  value: number,
  minValue: number = 0,
  maxValue: number = 0,
  allowedDecimals: number = 0,
): number {
  let currentValue = 0;
  if (!isNaN(value)) {
    if (!isNaN(allowedDecimals) && allowedDecimals > 0) {
      currentValue = parseFloat(
        value.toFixed(allowedDecimals < 4 ? allowedDecimals : 4),
      );
    } else {
      currentValue = value;
    }
  }
  return Math.min(Math.max(currentValue, minValue), maxValue);
}

export default limitValue;
