function validateSize(current: number = NaN, original: number): number {
  let currentSize = original;
  if (!isNaN(current)) {
    currentSize = current;
  }
  return currentSize;
}

export default validateSize;
